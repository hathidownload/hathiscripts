#!/bin/sh
# set these
username=SHIBUSERNAME
password=SHIBPASSWORD
shibidp=tufts

usage="$0 cookiedir"
test $# -ne 1 && echo "$usage" && exit 1

# specific to the Tufts cluster 
#module load python/2.7.3

rm -f "$1/cookies.txt"

shib-login -d "$1" -u "$username" -p "$password" https://babel.hathitrust.org/Shibboleth.sso/${shibidp}?target=https://babel.hathitrust.org/
