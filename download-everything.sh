#!/bin/sh
loginscript="/PATH/TO/hathi-login.sh"

usage="$0 bibdir downdir cookiedir
The bibdir should contain a list of .refer files which contain the IDs to download"

test $# -ne 3 && echo "$usage" && exit

bibdir=`readlink -f "$1"`
downdir=`readlink -f "$2"`
cookies=`readlink -f "$3"`/cookies.txt

for refer in "$bibdir"/*refer
do
	base=`basename "$refer" .refer`

	cat "$refer" | awk '/^UR / {print $0}' | sed 's/^UR  - //g' | sed '/catalog.hathitrust.org/d' | while read record
	do
		id=`echo "$record" | awk '{print $1}' | awk -F '/' '{print $5}'`
		name=`echo "$record" | sed 's/http[^ ]* //g' | sed 's/^(//g; s/)$//g'`
		if test -f "$downdir/$base/$name/$id.pdf"
		then
			echo "$base/$name/$id.pdf already exists, skipping"
			continue
		fi

		echo "Downloading $downdir/$base/$name/$id.pdf"
		mkdir -p "$downdir/$base/$name"
		wget --max-redirect 0 --no-verbose \
		     --load-cookies "$cookies" --save-cookies "$cookies" --keep-session-cookies \
		     -O "$downdir/$base/$name/$id.pdf" "https://babel.hathitrust.org/shcgi/imgsrv/download?id=$id"

		if test $? -ne 0
		then
			rm -f "$downdir/$base/$name/$id.pdf"
			sh "$loginscript" "$3"
		fi
	done
done
