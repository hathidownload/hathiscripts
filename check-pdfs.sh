#!/bin/sh
usage="$0 dir"

test $# -ne 1 && echo "$usage" && exit 1

find "$1" -type f | while read pdf
do
	pdfinfo "$pdf" > /dev/null 2>&1
	test $? -ne 0 && echo "$pdf looks invalid"
	#test $? -ne 0 && rm -f "$pdf"
done
